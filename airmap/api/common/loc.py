# -*- coding: utf-8 -*-

from flask import request
from flask_restful import Resource, reqparse
from airmap.models.loc import Loc
from airmap.core.errors import ApiError


parser = reqparse.RequestParser()
parser.add_argument('desc')
parser.add_argument('lat')
parser.add_argument('lng')

class CreateAndQueryLocAPI(Resource):
    def get(self):
        lat = request.args.get("lat", 39)
        lng = request.args.get("lng", 116)
        dist = request.args.get("dist", 1000)

        locs = Loc.query(lat, lng, dist)
        return [loc.full_dict() for loc in locs]

    def post(self):
        args = parser.parse_args()
        desc = args.desc
        lat = args.lat
        lng = args.lng

        loc = Loc.create(desc=desc,
                         lat=lat,
                         lng=lng)
        return loc.full_dict(), 201

class LocAPI(Resource):

    def get(self, id):
        loc = Loc.get_by_loc_id(id)
        if loc:
            return loc.full_dict()
        raise ApiError(ApiError.loc_not_found)

    def put(self, id):
        loc = Loc.get_by_loc_id(id)
        if loc:
            args = parser.parse_args()
            loc.update(
                       desc=args.desc or loc.desc,
                       lat=args.lat or loc.lat,
                       lng=args.lng or loc.lng)
            return loc.full_dict(), 201
        raise ApiError(ApiError.loc_not_found)

    def delete(self, id):
        loc = Loc.get_by_loc_id(id)
        if loc and loc.delete():
            return {'error': 0, 'message': 'delete success'}, 200
        raise ApiError(ApiError.loc_not_found)

