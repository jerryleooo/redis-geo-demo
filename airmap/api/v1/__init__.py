# -*- coding: utf-8 -*-

from flask import Blueprint
from flask.ext import restful

from airmap.api.common.loc import LocAPI, CreateAndQueryLocAPI


RESOURCE_URLS = [
    ("/loc", CreateAndQueryLocAPI, "loc.create_query"),
    ("/loc/<id>", LocAPI, "loc")
]


blueprint = Blueprint('api.v1', __name__, url_prefix='/api/v1')

def configure_api(blueprint):
    api = restful.Api(blueprint, catch_all_404s=True)

    for url, view, endpoint in RESOURCE_URLS:
        api.add_resource(view, url, endpoint=endpoint)

configure_api(blueprint)
