# -*- coding: utf-8 -*-

from flask_wtf.csrf import CsrfProtect
from flask_login import LoginManager
from flask_oauthlib.client import OAuth
from flask_cors import CORS
from flask_cache import Cache
from flask_debugtoolbar import DebugToolbarExtension
from flask_redis import FlaskRedis
from raven.contrib.flask import Sentry
from airmap import settings


csrf = CsrfProtect()
login_manager = LoginManager()
oauth = OAuth()
cors = CORS()
debug_toolbar = DebugToolbarExtension()
cache = Cache(config={'CACHE_TYPE': 'simple'})
redis_store = FlaskRedis()

sentry = Sentry(dsn=settings.SENTRY_DSN)

@csrf.error_handler
def csrf_error(reason):
    return reason, 400

@login_manager.user_loader
def load_user(user_id):
    from elftower.models.user import User
    return User.get_by_pk(user_id)

login_manager.login_view = 'public.login'
