# -*- coding: utf-8 -*-

from os import getenv

mode = getenv("AIRMAP_MODE") or "dev"

if mode == "dev":
    DEBUG = True
elif mode == "testing":
    TESTING = True
else:
    DEBUG = False
    TESTING = False

SENTRY_DSN = "http://662163ba907f4d4eb76494e3f892dded:4b7140303d0c42bb9b91433bdf871a69@sentry.instask.me:9000/3"
REDIS_URL = getenv("REDIS_URL") or "redis://localhost:6379"
