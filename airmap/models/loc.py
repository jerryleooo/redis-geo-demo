# -*- coding: utf-8 -*-

import uuid
from airmap.core.extensions import redis_store
from airmap.core.errors import ApiError


class Loc(object):
    __key__ = 'info:v3'

    def __init__(self, loc_id, desc='', lat=0, lng=0):
        self.loc_id = loc_id
        self.desc = desc
        self.lat = lat
        self.lng = lng

    @classmethod
    def gen_loc_id(self):
        return uuid.uuid4().hex

    def save(self):
        try:
            redis_store.geoadd(self.__key__, self.lng, self.lat, self.loc_id)
            redis_store.hmset(self.loc_id, self.info_dict())
            return self
        except Exception as e:
            raise e
            return None

    @classmethod
    def create(cls, **kwargs):
        loc_id = cls.gen_loc_id()
        kwargs.update({'loc_id': loc_id})
        instance = cls(**kwargs)
        return instance.save()

    def update(self, **kwargs):
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return self.save() or self

    @classmethod
    def get_by_loc_id(cls, loc_id):
        info = redis_store.hgetall(loc_id)
        lng, lat = cls.get_coordinate(loc_id)
        info.update({
            'loc_id': loc_id,
            'lat': lat,
            'lng': lng
        })
        instance = cls(**info)

        return instance

    @classmethod
    def get_coordinate(self, loc_id):
        try:
            return redis_store.geopos(self.__key__, loc_id)[0]
        except:
            raise ApiError(ApiError.loc_not_found)

    def delete(self):
        redis_store.hdel(self.loc_id, self.info_dict().keys())
        return redis_store.zrem(self.__key__, self.loc_id)

    @classmethod
    def query(cls, lat, lng, dist):
        loc_ids = redis_store.georadius(cls.__key__, lng, lat, dist)
        return [cls.get_by_loc_id(loc_id) for loc_id in loc_ids]

    def info_dict(self):
        return {
            "desc": self.desc,
        }

    def full_dict(self):
        d = self.info_dict()
        d.update({
            "id": self.loc_id,
            "lat": self.lat,
            "lng": self.lng,
        })
        return d

