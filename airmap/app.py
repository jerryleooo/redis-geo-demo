# -*- coding: utf-8 -*-


from flask import Flask, render_template, jsonify
from airmap.core.extensions import (login_manager, sentry, redis_store)
from airmap.api import v1
from airmap import settings
from airmap.core.errors import ApiError


import logging
logging.getLogger('flask_cors').level = logging.DEBUG

def create_app():
    app = Flask('airmap')
    app.config.from_object(settings)

    configure_blueprints(app)
    configure_extensions(app)
    configure_errorhandlers(app)
    configure_logging(app)
    # configure_after_request(app)

    return app

def configure_blueprints(app):
    for m in (v1, ):
        app.register_blueprint(m.blueprint)

def configure_extensions(app):
    login_manager.init_app(app)
    redis_store.init_app(app)
    if not app.testing:
        sentry.init_app(app)

def configure_errorhandlers(app):

    @app.errorhandler(400)
    def bad_request(error):
        return render_template("errors/400.html"), 400

    @app.errorhandler(401)
    def unauthorized_page(error):
        return render_template("public/login.html"), 401

    @app.errorhandler(403)
    def forbidden_page(error):
        return render_template("errors/403.html"), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("errors/404.html"), 404

    @app.errorhandler(500)
    def server_error_page(error):
        return render_template("errors/500.html"), 500

    @app.errorhandler(ApiError)
    def api_error_handler(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

def configure_logging(app):
    pass
