# -*- coding: utf-8 -*-

import json
from flask import url_for


def test_create_loc(app, redis_store):

    desc = 'test desc'
    lat = 40
    lng = 116

    with app.test_client() as c:
        res = c.post(
            url_for('api.v1.loc.create_query'),
            data={
                'desc': desc,
                'lat': lat,
                'lng': lng
            }
        )
        assert res.status_code == 201

        resp = json.loads(res.data)
        assert resp['desc'] == desc
        assert resp['id'] is not None

def test_query_loc(app, redis_store, loc):

    with app.test_client() as c:
        res = c.get(url_for('api.v1.loc.create_query', lat=loc.lat, lng=loc.lng))
        assert res.status_code == 200

        resp = json.loads(res.data)
        assert loc.full_dict()['lat'] - resp[0]['lat'] < 0.001
        assert loc.full_dict()['lng'] - resp[0]['lng'] < 0.001

def test_delete_loc(app, redis_store, loc):

    with app.test_client() as c:
        res = c.get(url_for('api.v1.loc.create_query', lat=loc.lat, lng=loc.lng))
        resp = json.loads(res.data)
        assert len(resp) == 1

        print loc.full_dict()
        res = c.delete(url_for('api.v1.loc', id=loc.loc_id))
        assert res.status_code == 200

        res = c.get(url_for('api.v1.loc.create_query', lat=loc.lat, lng=loc.lng))
        resp = json.loads(res.data)
        assert len(resp) == 0

def test_get_loc(app, redis_store, loc):
    with app.test_client() as c:
        res = c.get(url_for('api.v1.loc', id=loc.loc_id))
        resp = json.loads(res.data)
        assert res.status_code == 200
        assert resp['lat'] - loc.lat < 0.0001
        assert resp['desc'] == loc.desc

def test_update_loc(app, redis_store, loc):

    desc = 'test desc 2'

    with app.test_client() as c:
        res = c.put(url_for('api.v1.loc', id=loc.loc_id), data={
            'desc': desc,
        })
        resp = json.loads(res.data)
        assert res.status_code == 201
        assert resp['lat'] - loc.lat < 0.0001
        assert resp['desc'] == desc
