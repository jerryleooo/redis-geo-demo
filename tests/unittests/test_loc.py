# -*- coding: utf-8 -*-

from airmap.models.loc import Loc


def test_create_loc(app, redis_store):

    desc = 'test desc'
    lat = 40
    lng = 116

    loc = Loc.create(desc=desc,
                     lat=lat,
                     lng=lng)

    assert loc.desc == desc
    assert loc.lat == lat
    assert loc.lng == lng

def test_get_by_loc_id(redis_store, loc):
    another_loc = Loc.get_by_loc_id(loc.loc_id)
    assert another_loc.full_dict()['lat'] - loc.full_dict()['lat'] < 0.0001
    assert another_loc.full_dict()['lng'] - loc.full_dict()['lng'] < 0.0001
    assert another_loc.full_dict()['desc'] == loc.full_dict()['desc']


def test_delete(redis_store, loc):
    lat = loc.lat
    lng = loc.lng
    loc.delete()
    assert len(Loc.query(lat=lat, lng=lng, dist=10000)) == 0

