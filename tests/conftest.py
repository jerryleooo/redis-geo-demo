# -*- coding: utf-8 -*-

import pytest
from airmap.models.loc import Loc
from airmap.app import create_app
from airmap.core.extensions import redis_store as _redis


@pytest.yield_fixture()
def app():
    _app = create_app()
    ctx = _app.test_request_context()
    ctx.push()
    yield _app
    ctx.pop()

@pytest.yield_fixture()
def redis_store(app):
    _redis.init_app(app)
    yield _redis
    _redis.flushall()

@pytest.fixture
def loc():
    loc = Loc.create(desc='test desc',
                     lat=40,
                     lng=116)
    return loc
