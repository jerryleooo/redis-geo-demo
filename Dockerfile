FROM python:2.7
MAINTAINER Yang Liu <whilgeek@gmail.com>

RUN apt-get update && apt-get install netcat vim -y
ADD . /app
WORKDIR /app

RUN pip install --upgrade -r requirements.txt
RUN pip install --upgrade git+https://github.com/andymccurdy/redis-py.git@master
