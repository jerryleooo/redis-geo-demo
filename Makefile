.PHONY: clean install help test run dependencies

help:
	@echo "  clean              remove unwanted stuff"
	@echo "  install            install flaskbb and setup"
	@echo "  tests              run the testsuite"
	@echo "  run                run the development server"
	@echo "  deploy             deploy to www.instask.me"
	@echo "  upgrade            upgrade database of the www.instask.me"
	@echo "  ---------------------------------------------------------"
	@echo "  deploy to test     fab deploy_test:branch=branch_name"

dependencies:requirements.txt
	pip install -r requirements.txt

clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +

upgrade:
	dokku run instask python manage.py db upgrade

export:
	dokku postgres:export instask-database > /tmp/instask.dump

import:
	dokku postgres:import instask-test-database < /tmp/instask.dump

migrate_test:
	./bin/migration.sh

test:
	redis-server ./tests/test_redis.conf
	redis-cli -p 26379 flushall
	export REDIS_URL=redis://localhost:26379
	py.test --cov=airmap --cov-report=term-missing tests -vv
	redis-cli -p 26379 flushall
	redis-cli -p 26379 shutdown

run:
	python manage.py runserver -dr

install:dependencies
	clear
	python manage.py install

routes:
	python manage.py list_routes

deploy:
	fab deploy

